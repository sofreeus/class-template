# Creating a Class

---

## Thinking It Through

It's easy to get stuck trying to invent a story or project to captivate your students.  Sometimes you'll have a clear vision, but sometimes it's easier to answer the question, `What key concepts do my students need to know in order to get this done?`

1. What is the topic?
1. What project are we going to build to teach this topic?
1. What is the starting point?
  - Lab 00: Make sure all students are at this point.
1. What is the next step toward the goal?
  - Make a lab for this.
1. Repeat the last step until you're at your goal.

---

## Stay on the Critical Path

- If you think it's too simple, it's probably just right.
- Students who don't know the material...
  - Won't have complicated/esoteric questions.
  - Don't see the weird edge cases you're ignoring.
  - Will be fine with these answers:
    - "I don't know.  I'll have to research that and get back to you."
    - "I've never encountered that, but I'd be interested if your research turns up anything."
- If you want additional content, add `Bonus Labs`

---

## Using This Template

#### Option A - *Help!  I don't know Git!*

1. Open this template in one tab.
1. In a second tab, create a [new project](https://gitlab.com/projects/new) for your class
1. Click the `Web IDE` button in your class repo
1. Copy and paste from this template as necessary
1. To save your changes:
  1. Click the `Commit...` button
  1. Find and click the `Stage all changes` button
  1. Click the `Commit...` button (again)
  1. Select the `Commit to master branch` option
  1. Click the `Commit` button

#### Option B - *Git 'er done!*

1. Create a [new project](https://gitlab.com/projects/new) in Gitlab
1. Clone your class locally (e.g. `~/sfs/my-class`)
1. Download the template to your repo

  ```bash
  cd ~/sfs/my-class
  CLASS_TEMPLATE="https://gitlab.com/sofreeus/class-template/-/archive/master/class-template-master.tar"
  curl ${CLASS_TEMPLATE} | tar --strip-components 1 -xf -
  ```

#### Update the Lab Template

1. Review the [lab template](/labs/00_template/Readme.md)
1. Eliminate the optional sections you don't want to use
1. This will save you time since you copy it for each lab you create

---

## File Layout

- Utilizing `Readme.md` for your filenames helps facilitate using Gitlab as the presentation layer for your materials.  Gitlab will automatically render the `Readme.md` when you navigate to the containing folder.
- Numbering your lab folders helps you (and students) identify the intended order of progression.
  - Using two-digit numbering on your lab folders makes it easier to add labs (e.g. adding `15_git` between `10_bash` and `20_being_awesome`) without a ton of re-numbering.
- Naming the folders helps provide context.

```
my-class
├── Readme.md
└── labs
    ├── 00_basic_setup
    |   ├── Helpful_Image.png
    │   └── Readme.md
    ├── 10_first_thing
    |   ├── Another_Helpful_Image.png
    │   └── Readme.md
    ├── ...
    │   └── Readme.md
    └── N0_last_thing
        └── Readme.md
```

---

## Make It Your Own

- All the `Readme.md` files in this template are commented to guide you through the process of updating them.
- Create as many labs as you need.
- It's tempting to provide complete files in your class repo.
  - This is more complicated than you'd think.
  - Put your files inline in your `Readme.md`.
  - Don't make students compose whole files from snippets.  It's okay to show snippets, but show the whole file, too.

---

## Wrapping Up

**Important** - Ask students for feedback at the start of the last lab.

- [SFS Links](https://www.sofree.us/links/)
- [Feedback Form](https://bit.ly/sfsclassfeedback2019)
