<!-- Update the lab title -->
# Lab/Lesson Title

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Sub-Header

<!-- 3-4 sentences -->
<!-- Answer the following questions -->
- What are we going to do in this lab?
- How does it fit into the larger picture for this class?
  - Using images can help here.
- Why do we do it this way?
  - Stick to the Happy Path.
  - Don't dive into explanations of alternatives unless asked.

---

<!-- OPTIONAL Section -->
### Environment

- Where should students perform this lab?
  - File?
  - Path?
  - Site?
  - Repo?
- Are there variables that should be set?
  - API tokens?
  - SSH options?
  - A `NODE_ENV`?
- If you're maintaining `~/sfs/.env`...
  - Does it need updating?
  - What should it look like right now?

---

<!-- Update this section name -->
### First Logical Chunk

<!--
As you demo...
  explain what you're doing in each step
  explain what's happening in each step
If relevant...
  explain where each step fits into the overall workflow
  explain where the action is happening (e.g. local vs. remote web server)
-->

1. List the steps to complete the first logical chunk of this lab
1. Highlight `filenames` and `specific syntax` with inline backticks...
1. ...But avoid asking students to copy/paste/run `inline commands`
1. Use code blocks instead
  ```bash
  # use code blocks instead of inline commands
  echo "This can facilitate copy/paste by the students"
  ```
1. Add verification steps wherever possible
  ```bash
  grep ANOTHER_VAR ~/sfs/.env
  # should show:
  #   ANOTHER_VAR="something else"
  ```

---

<!-- OPTIONAL: Repeat as necessary -->
<!-- More than three logical chunks in a lab is a yellow flag -->
<!-- A lab with 4+ chunks could probably be split into multiple labs -->
<!-- Update this section name -->
### Next Logical Chunk

1. List the steps to complete the next logical chunk of this lab
1. Highlight `filenames` and `specific syntax` with inline backticks...
1. ...But avoid asking students to copy/paste/run `inline commands`
1. Use code blocks instead
  ```bash
  # use code blocks instead of inline commands
  echo "This can facilitate copy/paste by the students"
  ```
1. Add verification steps wherever possible
  ```bash
  grep ANOTHER_VAR ~/sfs/.env
  # should show:
  #   ANOTHER_VAR="something else"
  ```

---

<!-- OPTIONAL: References and Links -->
### References & Links
  - [Basic Markdown Syntax](https://www.markdownguide.org/basic-syntax)
  - [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)

---

<!-- OPTIONAL: Include a navigator at the bottom -->
<!-- This is very user-friendly, but a pain to maintain -->

|Previous: [Last Lab Title](/labs/lab_folder)|Next: [Next Lab Title](/labs/lab_folder)|
|---:|:---|
